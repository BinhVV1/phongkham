<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bacsi', function (Blueprint $table) {
            $table->id();
            $table->string("tenbacsi");
            $table->string("bangcap");
            $table->string("chuyenkhoa");
            $table->integer("sodienthoai");
            $table->string("hinhanh");
            $table->string("kinhnghiem");
            $table->longText("gioithieu");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bacsi');
    }
};
