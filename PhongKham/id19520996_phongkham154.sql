-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th3 30, 2023 lúc 02:43 AM
-- Phiên bản máy phục vụ: 10.5.16-MariaDB
-- Phiên bản PHP: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `id19520996_phongkham154`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bacsi`
--

CREATE TABLE `bacsi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tenbacsi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bangcap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chuyenkhoa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sodienthoai` int(11) NOT NULL,
  `hinhanh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kinhnghiem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gioithieu` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `bacsi`
--

INSERT INTO `bacsi` (`id`, `tenbacsi`, `bangcap`, `chuyenkhoa`, `sodienthoai`, `hinhanh`, `kinhnghiem`, `gioithieu`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn Văn A', 'Thạc Sĩ', 'Khoa Nội', 934567899, 'doctors-img1.png', '5', '\"Tôi chỉ là 1 Tiến sỹ - Bác sỹ bình thường trong số vài trăm Tiến sĩ của BV Việt Đức và tôi cũng chỉ thuộc dạng làng nhàng \"lơ lửng\" trong bệnh viện, cao cũng chẳng ra cao mà thấp cũng không phải thấp. \r\n\r\nTôi không biết những chia sẻ của tôi có truyền cảm hứng được cho các bạn trẻ mới vào ngành Y hay không. Nhưng tôi vẫn muốn thử bởi vì gần đây thi vào ngành Y vẫn khó quá, học tập lâu, vất vả và tốn kém quá. Nhưng ra trường nhiều bạn bỏ ngang, khó sống với ngành Y quá\". \r\n\r\nĐó là những lời nói đầu tiên mà TS. BS Trần Hoàng Tùng, Phó trưởng Khoa PT Chi dưới, Bệnh viện Hữu nghị Việt Đức chia sẻ với tôi khi mở đầu một câu chuyện dài... 19 tuổi tai nạn hỏng tay phải, sau 15 năm rèn luyện trở thành bác sĩ phẫu thuật nổi tiếng \"mát tay\".', '2023-03-21 14:39:51', NULL),
(2, 'Nguyễn Văn B', 'Tiến Sĩ', 'Khoa Ngoại', 123456789, 'doctors-img2.png', '4', '\"Tôi chỉ là 1 Tiến sỹ - Bác sỹ bình thường trong số vài trăm Tiến sĩ của BV Việt Đức và tôi cũng chỉ thuộc dạng làng nhàng \"lơ lửng\" trong bệnh viện, cao cũng chẳng ra cao mà thấp cũng không phải thấp. \n\nTôi không biết những chia sẻ của tôi có truyền cảm hứng được cho các bạn trẻ mới vào ngành Y hay không. Nhưng tôi vẫn muốn thử bởi vì gần đây thi vào ngành Y vẫn khó quá, học tập lâu, vất vả và tốn kém quá. Nhưng ra trường nhiều bạn bỏ ngang, khó sống với ngành Y quá\". \n\nĐó là những lời nói đầu tiên mà TS. BS Trần Hoàng Tùng, Phó trưởng Khoa PT Chi dưới, Bệnh viện Hữu nghị Việt Đức chia sẻ với tôi khi mở đầu một câu chuyện dài... 19 tuổi tai nạn hỏng tay phải, sau 15 năm rèn luyện trở thành bác sĩ phẫu thuật nổi tiếng \"mát tay\".', '2023-03-24 14:06:08', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `dichvu`
--

CREATE TABLE `dichvu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tendichvu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hinhanh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mota` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `noidung` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `dichvu`
--

INSERT INTO `dichvu` (`id`, `tendichvu`, `hinhanh`, `mota`, `noidung`, `created_at`, `updated_at`) VALUES
(1, 'KHÁM NỘI - KHOA NỘI', 'service-img1.jpg', 'Nội khoa là phân ngành trong y khoa liên quan đến việc chẩn đoán và điều trị không phẫu thuật các bệnh của cơ quan bên trong cơ thể, đặc biệt là ở người lớn.', 'Bệnh tim mạch hiện đang là gánh nặng cho xã hội với tỷ lệ tử vong và tàn phế cao hàng đầu trong các loại bệnh. Đặc biệt trong những năm gần đây, bệnh lý tăng huyết áp đang được trẻ hóa với rất nhiều đối tượng trong độ tuổi lao động. Do đó việc thăm khám, phát hiện sớm, chẩn đoán chính xác và điều trị kịp thời các bệnh tim mạch đóng vai trò rất quan trọng, làm giảm bớt những biến chứng nguy hiểm do bệnh tim mạch gây ra.\r\n\r\nVới sứ mệnh “Chăm sóc sức khỏe trọn đời cho bạn” BVĐK Hòa Bình cung cấp dịch vụ điều trị nội khoa tim mạch, phục vụ nhu cầu chăm sóc sức khỏe tim mạch của khách hàng.\r\n\r\nNội khoa là phân ngành trong y khoa liên quan đến việc chẩn đoán và điều trị không phẫu thuật các bệnh của cơ quan bên trong cơ thể, đặc biệt là ở người lớn. Nội khoa chủ yếu điều trị bằng thuốc, khác với ngoại khoa chủ yếu điều trị bằng phẫu thuật.\r\n\r\nTrong quá trình điều trị nội khoa tim mạch, các bác sĩ sẽ căn cứ vào tình hình sức khỏe của người bệnh, diễn tiến của bệnh để đưa ra hướng điều trị phù hợp nhất với tình trạng của họ.\r\n\r\nBVĐK Hòa Bình tập hợp đội ngũ bác sĩ, chuyên gia hàng đầu trong lĩnh vực tim mạch. Khi đến khám tại bệnh viện Hòa Bình, người bệnh sẽ được các bác sĩ giàu kinh nghiệm của chúng tôi trực tiếp thăm khám và tư vấn điều trị.\r\n\r\nBên cạnh đó, bệnh viện cũng thường xuyên chú trọng tới việc đầu tư, cập nhật các công nghệ chẩn đoán và phương pháp điều trị mới nhất, mang lại hiệu quả cao nhất để cung cấp cho khách hàng.\r\n\r\nHệ thống trang thiết bị y tế hiện đại đạt chuẩn quốc tế của bệnh viện không chỉ giúp phát hiện bệnh sớm ngay từ giai đoạn khởi đầu mà còn đóng vai trò quan trọng trong quá trình chẩn đoán và hỗ trợ điều trị.\r\n\r\nVới cơ sở vật chất khang trang, sạch đẹp, đội ngũ bác sĩ, nhân viên điều dưỡng chu đáo, nhiệt tình sẽ mang lại cho người bệnh cảm giác thoải mái, tự nhiên như đang ở nhà.', '2023-03-30 02:32:41', NULL),
(2, 'KHÁM NGOẠI - KHOA NGOẠI', 'service-img2.jpg', 'Khoa ngoại là một phân ngành nằm trong y học có liên quan đến việc chữa lành bệnh hoặc những tổn thương ở bên trong hay bên ngoài', '　　Bệnh ngoại khoa là những bệnh gây nên các rối loạn hoạt động hoặc làm thay đổi cấu trúc của các cơ quan trong cơ thể của người bệnh. Thường thì các thay đổi này sẽ gây ra các bệnh lý cần đến sự điều chỉnh bằng thuốc, các kỹ thuật mổ xẻ - may vá hay công nghệ y khoa nhằm loại bỏ hoặc sửa chữa các bộ phận bị hư hỏng và giúp cơ thể trở về trạng thái bình thường.', '2023-03-30 02:38:01', NULL),
(3, 'XQUANG-XÉT NGHIỆM-SIÊU ÂM-ĐIỆN TIM ', 'service-img3.jpg', 'Siêu âm tim là một thăm dò chẩn đoán bằng cách sử dụng sóng siêu âm tần số cao để có được những hình ảnh động về tim và những cấu trúc liên quan đến tim.', 'Bằng sóng siêu âm, bác sĩ có thể quan sát được cấu trúc tim; cách tim hoạt động, co bóp; kích thước tim, hình dạng tim; kích thước và chuyển động của các thành tim; sự hoạt động của van tim,...\r\n\r\nVới những thông tin này, bác sĩ sẽ có thể phát hiện và chẩn đoán các vấn đề gặp phải ở tim mạch như:\r\n\r\nBệnh lý về van tim: Hẹp van tim, hở van tim,... Đây là loại bệnh xảy ra khi cấu trúc của các lá van bị biến dạng, van tim không đóng kín, máu lưu thông ngược trở lại buồng tim.\r\nThay đổi kích thước tim: huyết áp cao hoặc các bệnh khác có thể làm cho các buồng tim và cơ tim tăng kích thước bất thường.\r\nTổn thương cơ tim: Siêu âm tim giúp bác sĩ phát hiện và chẩn đoán các bất thường trong quá trình tống máu. Giúp chẩn đoán và ngăn ngừa các biến chứng nguy hiểm khi hoại tử cơ tim kéo dài như nhồi máu cơ tim cấp.\r\nDị tật tim: Siêu âm tim có thể xác định bất thường tim bẩm sinh ở trẻ sơ sinh và trẻ nhỏ.\r\nTràn dịch màng tim: Tràn dịch màng ngoài tim là biểu hiện thường gặp của bệnh màng ngoài tim nguyên phát hoặc thứ phát do quá trình bệnh lý của cơ thể. Mức độ nghiêm trọng phụ thuộc vào tình trạng bệnh. Nếu không chữa trị, tràn dịch màng tim có thể gây ra suy tim.\r\nTheo dõi phương pháp điều trị các bệnh lý về tim: Theo dõi mức độ đáp ứng của tim đối với các phương pháp điều trị tim khác nhau như thuốc điều trị suy tim, van nhân tạo và máy tạo nhịp.', '2023-03-30 02:41:34', NULL),
(4, 'PHỤC HỒI CHỨC NĂNG - Y HỌC TRỊ LIỆU', 'service-img4.jpg', 'Vật lý trị liệu là phương pháp chữa bệnh không cần dùng thuốc. Bằng cách sử dụng các yếu tố vật lý như vận động cơ học, sóng âm, ánh sáng, nhiệt…', '1.1. Vai trò của vật lý trị liệu\r\nHỗ trợ giảm đau, giúp người bệnh giảm dần việc dùng thuốc điều trị để tránh các tác động bất lợi cho sức khỏe.\r\nTránh nguy cơ phẫu thuật đau đớn.\r\nCải thiện và phục hồi khả năng vận động sau chấn thương, bại liệt hay sau phẫu thuật, để người bệnh sớm trở lại sinh hoạt bình thường.\r\nThường xuyên thực hiện các phương pháp vật lý trị liệu còn giúp phòng ngừa nhiều vấn đề sức khỏe liên quan đến tuổi tác.\r\n1.2. Các hình thức, kỹ thuật vật lý trị liệu\r\nCó rất nhiều hình thức/kỹ thuật vật lý trị liệu nhưng chủ yếu phân thành 2 nhóm chính như sau:\r\n\r\nVật lý trị liệu chủ động: Những bài tập được thiết kế để tập với công cụ đi kèm hoặc đơn giản là những bài tập như đi bộ, đạp xe…\r\n\r\nVật lý trị liệu bị động: Bao gồm trị liệu bằng nhiệt, trị liệu bằng ánh sáng hay nước, kích thích điện, dùng sóng âm, điều trị bằng siêu âm, nắn hoặc xoa bóp bằng tay… giúp giải phóng các áp lực chèn ép rễ dây thần kinh và đẩy nhanh quá trình tái tạo mô tổn thương.\r\n\r\nKhông chỉ có đội ngũ bác sĩ, kỹ thuật viên giàu kinh nghiệm, tất cả phòng khám ACC đều trang bị hệ thống vật lý trị liệu hiện đại với sự hỗ trợ của nhiều thiết bị tiên tiến như: Máy kéo giãn giảm áp cột sống DTS có hiệu quả tối ưu trong hội chứng đĩa đệm và thoái hóa cột sống; máy vận động trị liệu tích cực ATM2 giúp làm dịu các cơn đau, điều chỉnh xoay chiều cột sống thẳng hiệu quả; sóng xung kích Shockwave có tác dụng giảm đau và thúc đẩy quá trình phục hồi mô, tế bào.\r\n\r\nNgoài ra, ACC còn sử dụng thiết bị giảm áp Vertetrac và Cervico 2000, tia laser cường độ cao thế hệ thứ IV, là những giải pháp hỗ trợ điều trị dứt điểm các bệnh về xương khớp.', '2023-03-30 02:33:58', NULL),
(5, 'Y HỌC CỔ TRUYỀN ', 'service-img5.jpg', 'Y học cổ truyền là một thuật ngữ được sử dụng để chỉ ngành y tế có sự  liên hệ mật thiết  với các nước Đông Á.', 'Các phương pháp điều trị  Y học Cổ truyền\r\nY học cổ truyền dựa trên sự cân bằng, hài hòa về năng lượng.Khái niệm cơ bản của nó dựa trên động lực quan trọng của sự sống, được gọi là Qi(khí), tràn khắp cơ thể. Bất kỳ sự mất cân bằng nào đối với Khí đều có thể gây ra bệnh tật. Phương pháp điều trị bằng Y học cổ truyền tìm cách khôi phục sự cân bằng này thông qua phương pháp điều trị dành riêng cho từng cá nhân như :\r\n\r\nChâm cứu.\r\nGiác hơi.\r\nThảo dược\r\nNgải cứu.\r\nThái cực quyền.', '2023-03-30 02:34:29', NULL),
(6, 'HỔ TRỢ Y TẾ CƠ QUAN - DOANH NGHIỆP', 'service-img6.jpg', 'Khám sức khỏe định kỳ cho nhân viên mang lại lợi ích cho cả bản thân người lao động và doanh nghiệp. ', 'Khám sức khỏe định kỳ giúp phát hiện sớm các bất thường về sức khỏe trước khi chuyển thành bệnh hoặc bệnh đang ở giai đoạn sớm chưa biểu hiện ra ngoài. Việc phát hiện bệnh ở giai đoạn sớm giúp việc điều trị dễ dàng, hiệu quả hơn, tiết kiệm chi phí và tránh các biến chứng ảnh hưởng nghiêm trọng đến sức khỏe.\r\n\r\nCác công ty, doanh nghiệp phải có trách nhiệm khám sức khỏe định kỳ cho nhân viên, điều này đã được quy định cụ thể trong nhiều văn bản luật.\r\n\r\nLuật lao động 2012, điều 152 về chăm sóc sức khỏe người lao động quy định:\r\n\r\nNgười sử dụng lao động hàng năm phải tổ chức khám sức khỏe định kỳ cho người lao động, kể cả người học nghề, tập nghề. Riêng lao động nữ phải được khám thêm chuyên khoa phụ sản. Riêng đối với những người làm công việc nặng nhọc, độc hại, người lao động là người chưa thành niên, người cao tuổi, người khuyết tật phải được khám sức khỏe ít nhất 6 tháng một lần.\r\nNgười lao động làm việc trong môi trường có nguy cơ mắc bệnh nghề nghiệp phải được khám bệnh nghề nghiệp. Người lao động sau khi bị tai nạn lao động, mắc bệnh nghề nghiệp nếu còn tiếp tục làm việc thì được sắp xếp công việc phù hợp với sức khỏe.\r\nTương tự như trên, Luật an toàn vệ sinh lao động năm 2015, điều 21 cũng nêu rõ các quy định về khám sức khỏe cho người lao động:\r\n\r\nÍt nhất một lần trong năm người sử dụng lao động phải tổ chức khám sức khỏe cho người lao động. Khám sức khỏe ít nhất 6 tháng một lần đối với người lao động là người khuyết tật, người cao tuổi, người chưa thành niên, người làm các nghề, công việc nặng nhọc, nguy hiểm, độc hại phải được.\r\nDanh mục các nghề, công việc nặng nhọc, độc hại, nguy hiểm được quy định mới nhất theo Thông tư 15/2016/TT-BLĐTBXH của Bộ lao động thương binh và xã hội.\r\n\r\nBên cạnh đó, lao động nữ phải được khám thêm chuyên khoa phụ sản. Người làm việc trong môi trường lao động tiếp xúc với các yếu tố nguy cơ gây bệnh nghề nghiệp phải được khám để phát hiện bệnh.\r\nTrước khi bố trí làm việc và trước khi chuyển sang làm nghề, công việc nặng nhọc, độc hại, nguy hiểm hơn các công ty, doanh nghiệp phải tổ chức khám sức khỏe cho người lao động. Sau khi bị tai nạn lao động, mắc bệnh nghề nghiệp đã phục hồi sức khỏe, người lao động cũng được khám để đảm bảo đủ sức khỏe trước khi tiếp tục trở lại làm việc.', '2023-03-30 02:42:04', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_03_21_130648_create_dichvu_table', 1),
(6, '2023_03_21_131450_create_bacsi_table', 1),
(7, '2023_03_21_134630_create_tintuc_table', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tintuc`
--

CREATE TABLE `tintuc` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tieude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hinhanh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tintuc`
--

INSERT INTO `tintuc` (`id`, `tieude`, `hinhanh`, `mota`, `link`, `created_at`, `updated_at`) VALUES
(1, 'Quy định về khám sức khỏe định kỳ cho người lao động theo thông tư mới nhất', 'blog-img.jpg', 'Khám sức khỏe định kỳ cho nhân viên mang lại lợi ích cho cả bản thân người lao động và doanh nghiệp. Qua khám sức khỏe định kỳ, nhân viên có thể phát hiện sớm các bất thường về sức khỏe để điều trị kịp thời, an tâm lao động và sản xuất. Bên cạnh đó, doanh', 'https://www.vinmec.com/vi/tin-tuc/thong-tin-suc-khoe/suc-khoe-tong-quat/quy-dinh-ve-kham-suc-khoe-dinh-ky-cho-nguoi-lao-dong-theo-thong-tu-moi-nhat/', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `bacsi`
--
ALTER TABLE `bacsi`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `dichvu`
--
ALTER TABLE `dichvu`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Chỉ mục cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Chỉ mục cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `bacsi`
--
ALTER TABLE `bacsi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `dichvu`
--
ALTER TABLE `dichvu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
