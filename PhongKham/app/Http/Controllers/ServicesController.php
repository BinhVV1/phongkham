<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DichVu;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = DichVu::all();
        return view('frontend.services.services',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function detail($id)
    {
        $detail = DichVu::where('id',$id)->get()->toArray();
        $data2 = DichVu::where('id','<>',$id)->get()->toArray();
        return view('frontend.services.services-detail',compact('detail','data2'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
