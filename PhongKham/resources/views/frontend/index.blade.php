@extends('frontend.layouts.app')
 @section('content')
 <!-- Banner Area -->
 <div class="banner-area banner-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="banner-content">
                    <span>Ngôi nhà của hy vọng của bạn</span>
                    <h1>Tận Tâm - Tận Lực Trách Nhiệm</h1>
                    <p>Nhận cuộc hẹn của bạn thông qua trực tuyến và giữ an toàn tại nhà của bạn. Bởi vì sự an toàn của bạn là ưu tiên hàng đầu của chúng tôi.</p>
                    <div class="banner-btn">
                        <a href="{{route('contact')}}" class="appointment-btn">Liên Hệ Với Chúng Tôi</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="banner-img">
        <img src="{{asset('frontend/img/home-one/home-one-img.png')}}" alt="Images">
    </div>
    <div class="banner-shape">
        <div class="shape1">
            <img src="{{asset('frontend/img/home-one/shape1.png')}}" alt="Images">
        </div>
        <div class="shape2">
            <img src="{{asset('frontend/img/home-one/shape2.png')}}" alt="Images">
        </div>
    </div>
</div>
<!-- Banner Area End -->

<!-- Banner Bottom -->
<div class="banner-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="banner-bottom-card">
                    <i class='flaticon-call'></i>
                    <div class="content">
                        <span>Nhận dịch vụ khẩn cấp 24/7</span>
                        <h1><a href="tel:+84826154154">082 615 4154</a></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Banner Bottom End -->

<!-- About Area -->
<div class="about-area pt-100 pb-70">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="about-img">
                    <img src="{{asset('frontend/img/about-img/about-img.jpg')}}" alt="Images">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="about-content">
                    <div class="section-title">
                        <span>Về chúng tôi</span>
                        <h2>Chúng tôi là người bạn đáng tin cậy của bạn</h2>
                        <p>Phòng khám 154 là một nơi đáng tin cậy của Dịch vụ y tế luôn ở bên cạnh bạn và sức khỏe của bạn là ưu tiên hàng đầu của chúng tôi.</p>
                        <p>
                        Phòng khám 154 sẽ được quản lý thông qua các chương trình có thể tùy chỉnh dựa trên kế hoạch kết hợp sự hợp tác giữa các thành viên gia đình và người chăm sóc đối với bệnh dài hạn hoặc quản lý bệnh.
                        </p>
                    </div>
                    <!-- <div class="about-card">
                        <i class='flaticon-24-hours bg-one'></i>
                        <div class="content">
                            <span>24/7 Support</span>
                            <p>Our medical team of  different department for long term illness writers and editors makes all the </p>
                        </div>
                    </div>

                    <div class="about-card">
                        <i class='flaticon-ambulance-2 bg-two'></i>
                        <div class="content">
                            <span>Emergency Support</span>
                            <p>Our medical team of  different department for long term illness writers and editors makes all the</p>
                        </div>
                    </div> -->

                    <div class="about-btn">
                        <a href="{{route('about')}}" class="default-btn">Xem Thêm</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About Area End -->

<!-- Appointment Area -->
<div class="appointment-area appointment-bg pt-100 pb-30">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xxl-5">
            </div>
        </div>
    </div>
        <div class="appointment-shape">
            <img src="{{asset('frontend/img/appointment/appointment-shape.png')}}" alt="Images">
        </div>
</div>
<!-- Appointment Area End -->

<!-- Service Area -->
<section class="service-area pt-100 pb-70">
    <div class="container">
        <div class="section-title text-center">
            <h2>Dịch vụ mà chúng tôi cung cấp</h2>
            <div class="section-icon">
                <div class="icon">
                    <i class="flaticon-dna"></i>
                </div>
            </div>
            <p>
            Chúng tôi cung cấp các dịch vụ tuyệt vời cho sức khỏe tốt nhất của bạn. Dưới đây là một số dịch vụ được bao gồm để bạn hiểu rõ hơn rằng chúng tôi luôn ở bên bạn.
            </p>
        </div>
        <div class="row pt-45">
        @if(!empty($dichvu))
            @foreach($dichvu as $val)
            <div class="col-lg-4 col-md-6">
                <div class="service-card">
                    <a href="{{route('services-detail',['id'=>$val['id']])}}"><img src="{{asset('frontend/img/services/'.$val['hinhanh'].'')}}" alt="Images"></a>
                    <div class="service-content">
                        <div class="service-icon">
                            <i class="flaticon-doctor"></i>
                        </div>
                        <h3><a href="{{route('services-detail',['id'=>$val['id']])}}">{{$val['tendichvu']}}</a></h3>
                        <div class="content">
                            <p>{{$val['mota']}}</p>
                        </div>
                    </div>
                    <div class="service-shape-1">
                        <img src="{{asset('frontend/img/services/service-shape1.png')}}" alt="Images">
                    </div>
                    <div class="service-shape-2">
                        <img src="{{asset('frontend/img/services/service-shape2.png')}}" alt="Images">
                    </div>
                </div>
            </div>

            @endforeach
            @else
               <p style='margin:auto;'> Hiện Chưa Có Danh Sách Dịch Vụ</p>
            @endif
            
        </div>
    </div>
    <div class="service-dots">
        <img src="{{asset('frontend/img/services/service-dots.png')}}" alt="">
    </div>
</section>
<!-- Service Area End -->


<!-- Doctors Area -->
<div class="doctors-area ptb-100">
    <div class="container">
        <div class="section-title text-center">
            <h2>Gặp gỡ chuyên gia của chúng tôi</h2>
            <div class="section-icon">
                <div class="icon">
                    <i class="flaticon-dna"></i>
                </div>
            </div>
            <p>
            Chúng tôi cung cấp các dịch vụ tuyệt vời cho sức khỏe tốt nhất của bạn. Dưới đây là một số dịch vụ được bao gồm để bạn hiểu rõ hơn rằng chúng tôi luôn ở bên bạn.
            </p>
        </div>
        <div class="doctors-slider owl-carousel owl-theme pt-45">
            @if(!empty($bacsi))
            @foreach($bacsi as $val)
            <div class="doctors-item">
                <div class="doctors-img">
                    <a href="{{route('doctor-detail',['id'=>$val['id']])}}">
                        <img src="{{asset('frontend/img/doctors/'.$val['hinhanh'].'')}}" alt="Images">
                    </a>
                </div>
                <div class="content">
                    <h3><a href="{{route('doctor-detail',['id'=>$val['id']])}}">{{$val['tenbacsi']}}</a></h3>
                    <span>{{$val['bangcap']}} </span> 
                    <ul class="social-link">
                        <li>
                            <a href="#" target="_blank"><i class='bx bxl-facebook'></i></a>
                        </li> 
                        <li>
                            <a href="#" target="_blank"><i class='bx bxl-twitter'></i></a>
                        </li> 
                        <li>
                            <a href="#" target="_blank"><i class='bx bxl-instagram'></i></a>
                        </li> 
                        <li>
                            <a href="#" target="_blank"><i class='bx bxl-linkedin'></i></a>
                        </li> 
                    </ul>
                </div>
            </div>
            @endforeach
            @else
               <p style='margin:auto;'> Hiện Chưa Có Danh Sách Bác Sĩ</p>
            @endif
            
        </div>
    </div>

    <div class="doctors-shape">
        <div class="doctors-shape-1">
            <img src="{{asset('frontend/img/doctors/doctors-shape1.png')}}" alt="Images">
        </div>
        <div class="doctors-shape-2">
            <img src="{{asset('frontend/img/doctors/doctors-shape2.png')}}" alt="Images">
        </div>
    </div>
</div>
<!-- Doctors Area End -->

<!-- Emergency Area -->
<div class="emergency-area ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="emergency-content">
                    <h2>Nhận dịch vụ chăm sóc <b> khẩn cấp 24/7 </b> </h2>
                    <p>Chúng tôi luôn ở bên cạnh bạn. Chúng tôi luôn sẵn sàng 24 giờ cho bạn trong trường hợp khẩn cấp.</p>
                    <div class="emergency-icon-content">
                        <i class="flaticon-24-hours-1"></i>
                        <h3><a href="tel:+84826154154">082 615 4154</a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="emergency-shape">
        <img src="{{asset('frontend/img/emergency/emergency-shape.png')}}" alt="Images">
    </div>
</div>
<!-- Emergency Area End -->

<!-- Blog Area -->
<div class="blog-area pt-100 pb-70">
    <div class="container">
        <div class="section-title text-center">
            <h2>Tin tức & Cập nhật của chúng tôi</h2>
            <div class="section-icon">
                <div class="icon">
                    <i class="flaticon-dna"></i>
                </div>
            </div>
            <p>
            Chúng tôi cung cấp các dịch vụ tuyệt vời cho sức khỏe tốt nhất của bạn. Dưới đây là một số dịch vụ được bao gồm để bạn hiểu rõ hơn rằng chúng tôi luôn ở bên bạn.
            </p>
        </div>
        <div class="row pt-45">
            <div class="col-lg-4 col-md-6">
                <div class="blog-card">
                    <a href="">
                        <img src="{{asset('frontend/img/blog/blog-img.jpg')}}" alt="Images">
                    </a>
                    <div class="content">
                        <ul>
                            <li>
                                <i class="flaticon-calendar-1"></i>
                                August 31, 2020
                                <span>
                                    <a href="#">Healthcare</a>
                                </span>
                            </li>
                        </ul>
                        <h3>
                            <a href=""> Lockdowns Leads to Fewer Peo - Ple Seeking Medical Care</a>
                        </h3>
                        <p>Victoria’s State of Emergency (SOE) measures resulted in almost 40 per cent less people prese nting to Alfred Health’s....</p>
                        <a href="" class="more-btn">
                            Read More <i class="flaticon-right-arrow"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="blog-card">
                    <a href="">
                        <img src="{{asset('frontend/img/blog/blog-img.jpg')}}" alt="Images">
                    </a>
                    <div class="content">
                        <ul>
                            <li>
                                <i class="flaticon-calendar-1"></i>
                                August 24, 2020
                                <span>
                                    <a href="#">Medicine</a>
                                </span>
                            </li>
                        </ul>
                        <h3>
                            <a href=""> Emergency Medicine Research Course for the Doctors</a>
                        </h3>
                        <p>Victoria’s State of Emergency (SOE) measures resulted in almost 40 per cent less people prese nting to Alfred Health’s....</p>
                        <a href="" class="more-btn">
                            Read More <i class="flaticon-right-arrow"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 offset-lg-0 offset-md-3">
                <div class="blog-card-side">
                    <ul>
                        <li>
                            <i class="flaticon-calendar-1"></i>
                            August 24, 2020
                            <span>
                                <a href="#">Medicine</a>
                            </span>
                            <a href="">
                                <h3> Advance Care Planning Information Session - 2020</h3>
                            </a>
                        </li>

                        <li>
                            <i class="flaticon-calendar-1"></i>
                            August 30, 2020
                            <span>
                                <a href="#">Medicine</a>
                            </span>
                            <a href="">
                                <h3> New Hope for Brain Cancer Treatment in the World</h3>
                            </a>
                        </li>

                        <li>
                            <i class="flaticon-calendar-1"></i>
                            September 10, 2020
                            <span>
                                <a href="#">Medicine</a>
                            </span>
                            <a href="">
                                <h3> Drug Trial Aims to Clear Covid-19 Virus Faster</h3>
                            </a>
                        </li>

                        <li>
                            <i class="flaticon-calendar-1"></i>
                            September 20, 2020
                            <span>
                                <a href="#">Medicine</a>
                            </span>
                            <a href="">
                                <h3> Clinic to Measure Heart Health a After Covid-19</h3>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

           
        </div>
    </div>
    <div class="blog-shape-icon">
        <i class="flaticon-dna"></i>
    </div>
</div>
<!-- Blog Area End -->

<!-- Testimonials Area -->
<div class="testimonials-area pt-100 pb-70">
    <div class="container">
        <div class="section-title text-center">
        <span>Đánh Giá</span>
                    <h2>Suy nghĩ của bệnh nhân của chúng tôi</h2>
        </div>
        <div class="row pt-45">
            <div class="col-lg-6">
                <div class="testimonials-img">
                    <img src="{{asset('frontend/img/testimonials/testimonials-img.jpg')}}" alt="Images">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="testimonials-slider-area">
                    <div class="testimonials-slider owl-carousel owl-theme">
                        <div class="testimonials-item">
                            <i class="flaticon-left-quote"></i>
                            <p>Medizo hospital for their professional competent and committed approach they deliver at all times, making me and other patients feel at ease during these anxious times. </p>
                            <div class="content">
                                <img src="{{asset('frontend/img/testimonials/testimonials-client1.jpg')}}" alt="Images">
                                <h3>Sinthy Alina</h3>
                                <span>Heart Patient</span>
                            </div>
                        </div>

                        <div class="testimonials-item">
                            <i class="flaticon-left-quote"></i>
                            <p>Medizo hospital for their professional competent and committed approach they deliver at all times, making me and other patients feel at ease during these anxious times. </p>
                            <div class="content">
                                <img src="{{asset('frontend/img/testimonials/testimonials-client1.jpg')}}" alt="Images">
                                <h3>Evanaa</h3>
                                <span>Diabetic Patient</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Testimonials Area End -->

<!-- Brand Area -->
<!-- <div class="brand-area brand-bg">
    <div class="container">
        <div class="brand-slider owl-carousel owl-theme">
            <div class="brand-item">
                <img src="{{asset('frontend/img/brand/brand-img1.png')}}" class="brand-logo-one" alt="Images">
                <img src="{{asset('frontend/img/brand/brand-logo1.png')}}" class="brand-logo-two" alt="Images">
            </div>
            <div class="brand-item">
                <img src="{{asset('frontend/img/brand/brand-img2.png')}}" class="brand-logo-one" alt="Images">
                <img src="{{asset('frontend/img/brand/brand-logo2.png')}}" class="brand-logo-two" alt="Images">
            </div>
            <div class="brand-item">
                <img src="{{asset('frontend/img/brand/brand-img3.png')}}" class="brand-logo-one" alt="Images">
                <img src="{{asset('frontend/img/brand/brand-logo3.png')}}" class="brand-logo-two" alt="Images">
            </div>
            <div class="brand-item">
                <img src="{{asset('frontend/img/brand/brand-img4.png')}}" class="brand-logo-one" alt="Images">
                <img src="{{asset('frontend/img/brand/brand-logo4.png')}}" class="brand-logo-two" alt="Images">
            </div>
            <div class="brand-item">
                <img src="{{asset('frontend/img/brand/brand-img5.png')}}" class="brand-logo-one" alt="Images">
                <img src="{{asset('frontend/img/brand/brand-logo5.png')}}" class="brand-logo-two" alt="Images">
            </div>
            <div class="brand-item">
                <img src="{{asset('frontend/img/brand/brand-img6.png')}}" class="brand-logo-one" alt="Images">
                <img src="{{asset('frontend/img/brand/brand-logo6.png')}}" class="brand-logo-two" alt="Images">
            </div>
            <div class="brand-item">
                <img src="{{asset('frontend/img/brand/brand-img3.png')}}" class="brand-logo-one" alt="Images">
                <img src="{{asset('frontend/img/brand/brand-logo3.png')}}" class="brand-logo-two" alt="Images">
            </div>
        </div>
    </div>
</div> -->
<!-- Brand Area End -->

<!-- Subscribe Area -->
<div class="subscribe-area ptb-100">
    <div class="subscribe-shape">
        <img src="{{asset('frontend/img/subscribe-img/subscribe-shape.png')}}" alt="Images">
    </div>
</div>
<!-- Subscribe Area End -->

<!-- Footer Area -->
@endsection