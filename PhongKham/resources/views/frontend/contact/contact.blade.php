@extends('frontend.layouts.app')
@section('content')
<div class="inner-banner inner-bg2">
            <div class="container">
                <div class="inner-title">
                    <h3>Liên Hệ</h3>
                    <ul>
                        <li>
                            <a href="/">Trang Chủ</a>
                        </li>
                        <li>Liên Hệ</li>
                    </ul>
                </div>
            </div>
            <div class="inner-banner-shape">
                <div class="shape1">
                    <img src={{asset('frontend/img/inner-banner/inner-banner-shape1.png')}}" alt="Images">
                </div>
                <div class="shape2">
                    <img src={{asset('frontend/img/inner-banner/inner-banner-shape2.png')}}" alt="Images">
                </div>
            </div>
        </div>
        <!-- Inner Banner End -->

        <!-- Contact Area -->
        <div class="contact-area pt-100 pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="contact-widget-right">
                            <h3>Thông Tin Liên Hệ</h3>
                            <p>Chúng tôi luôn ở bên cạnh bạn. Chúng tôi luôn sẵn sàng 24 giờ cho bạn trong trường hợp khẩn cấp.p>
                            <ul class="contact-list">
                                <li>
                                    <i class='flaticon-pin'></i>
                                    <div class="content">
                                    144 Lê Lợi, Đông Hà, Quảng Trị, Việt Nam
                                    </div>
                                </li>
                                <li>
                                    <i class='flaticon-phone-call'></i>
                                    <div class="content">
                                    <a href="tel:+84826154154">082 615 4154</a>
                                    </div>
                                </li>
                                <li>
                                    <i class='bx bxs-envelope'></i>
                                    <div class="content">
                                    <a href="mailto:Phongkham154@gmail.com">Phongkham154@gmail.com </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-8">
                        <div class="contact-form">
                            <h2>Google Map</h2>
                            <div class="map-area">
                            <div class="container-fluid m-0 p-0">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3819.3891069747187!2d107.10937075066018!3d16.807042023648997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3140e58ff5518f2d%3A0xc5940615301fc688!2zMTQ0IEzDqiBM4bujaSwgxJDDtG5nIEzhu4UsIMSQw7RuZyBIw6AsIFF14bqjbmcgVHLhu4ssIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1679673587422!5m2!1svi!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact-shape">
                <img src={{asset('frontend/img/shape/shape2.png')}}" alt="Images">
            </div>
        </div>
        <!-- Contact Area End -->

        <!-- Map Area -->
        
@endsection