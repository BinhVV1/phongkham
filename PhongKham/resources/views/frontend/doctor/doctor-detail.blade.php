@extends('frontend.layouts.app')
 @section('content')
<div class="doctors-details-area pt-100 pb-70">
            <div class="container">
                <div class="row align-items-center">
                    @if(!empty($data)||isset($data))
                            @foreach($data as $val)
                        
                        <div class="col-lg-4">
                            <div class="doctors-details-img">
                                <img src="{{asset('frontend/img/doctors/'.$val['hinhanh'].'')}}" alt="Images">
                            </div>
                        </div>
                    
                        <div class="col-lg-8">
                            <div class="doctors-details-content">
                                <h3>{{$val['tenbacsi']}}</h3>
                                <span>{{$val['bangcap']}}</span>
                                <ul class="doctors-details-list">
                                    <li>Chuyên Khoa: {{$val['chuyenkhoa']}}</li>
                                    <li>Kinh Nghiệm: {{$val['kinhnghiem']}} năm</li>
                                    <li>Số Điện Thoại: <a href="tel:+84{{$val['sodienthoai']}}">0{{$val['sodienthoai']}}</a></li>
                                </ul>

                                <ul class="social-link">
                                    <li class="title">Follow On :</li>
                                    <li>
                                        <a href="" target="_blank"><i class='bx bxl-facebook'></i></a>
                                    </li> 
                                    <li>
                                        <a href="" target="_blank"><i class='bx bxl-twitter'></i></a>
                                    </li> 
                                    <li>
                                        <a href="" target="_blank"><i class='bx bxl-instagram'></i></a>
                                    </li> 
                                    <li>
                                        <a href="" target="_blank"><i class='bx bxl-linkedin'></i></a>
                                    </li> 
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="doctors-details-text">
                                <p>
                                {{$val['gioithieu']}}
                                </p>
                            </div>
                        </div>
                    @endforeach
                    @else
                    <span>Hiện Chưa Có Thông Tin Bác Sĩ</span>
                    @endif
                </div>
            </div>
            <div class="doctors-details-shape">
                <img src="{{asset('frontend/img/doctors/doctors-shape4.png')}}" alt="Images">
            </div>
        </div>
        <!-- Doctors Details Area End -->

        <!-- Appointment Area -->
        <div class="subscribe-area ptb-100">
            <div class="subscribe-shape">
                <!-- <img src="{{asset('frontend/img/subscribe-img/subscribe-shape.png')}}" alt="Images"> -->
            </div>
        </div>
@endsection