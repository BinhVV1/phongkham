@extends('frontend.layouts.app')
 @section('content')
<div class="inner-banner inner-bg9">
            <div class="container">
                <div class="inner-title">
                    <h3>Bác Sĩ</h3>
                    <ul>
                        <li>
                            <a href="/">Trang Chủ  </a>
                        </li>
                        <li>Bác Sĩ</li>
                    </ul>
                </div>
            </div>
            <div class="inner-banner-shape">
                <div class="shape1">
                    <img src="frontend/img/inner-banner/inner-banner-shape1.png" alt="Images">
                </div>
                <div class="shape2">
                    <img src="frontend/img/inner-banner/inner-banner-shape2.png" alt="Images">
                </div>
            </div>
        </div>
        <!-- Inner Banner End -->

        <!-- Doctor Tab Area -->
        <div class="doctor-tab-area pt-100 pb-70">
            <div class="container">
                <div class="tab doctor-tab text-center">
                    <div class="tab_content current active pt-45">
                        <div class="tabs_item current">
                            <div class="doctor-tab-item">
                                <div class="row">
                                @if(!empty($data)||isset($data))
                                    @foreach($data as $val)
                                    <div class="col-lg-4 col-md-6">
                                        <div class="doctors-item">
                                            <div class="doctors-img">
                                                <a href="{{route('doctor-detail',['id'=>$val['id']])}}">
                                                    <img src="frontend/img/doctors/{{$val['hinhanh']}}" alt="Images">
                                                </a>
                                            </div>
                                            <div class="content">
                                                <h3><a href="{{route('doctor-detail',['id'=>$val['id']])}}">{{$val['tenbacsi']}}</a></h3>
                                                <span>{{$val['bangcap']}}</span> 
                                                <ul class="social-link">
                                                    <li>
                                                        <a href="#" target="_blank"><i class='bx bxl-facebook'></i></a>
                                                    </li> 
                                                    <li>
                                                        <a href="#" target="_blank"><i class='bx bxl-twitter'></i></a>
                                                    </li> 
                                                    <li>
                                                        <a href="#" target="_blank"><i class='bx bxl-instagram'></i></a>
                                                    </li> 
                                                    <li>
                                                        <a href="#" target="_blank"><i class='bx bxl-linkedin'></i></a>
                                                    </li> 
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @else
                                <span>Hiện chưa có danh sách bác sĩ</span>
                                @endif
                                    

                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="doctor-tab-shape">
                <div class="shape1">
                    <img src="frontend/img/doctors/doctors-shape3.png" alt="Images">
                </div>

                <div class="shape2">
                    <img src="frontend/img/doctors/doctors-shape4.png" alt="Images">
                </div>
            </div>
        </div>
        <!-- Doctor Tab Area End -->

        <!-- Appointment Area -->
        <!-- <div class="appointment-area appointment-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="appointment-from-area">
                            <div class="appointment-from ">
                                <h2>Get Your Appointment</h2>
                                <p>Online Easily During This Corona Pandemic</p>
                                <form>
                                    <div class="row">
                                        <div class="col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option value="">Select Departments</option>
                                                    <option value="">Dental Care</option>
                                                    <option value="">Cardiology</option>
                                                    <option value="">Neurology</option>
                                                    <option value="">Orthopedics</option>
                                                    <option value="">Medicine</option>
                                                </select>	
                                            </div>
                                        </div>
    
                                        <div class="col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option value="">Select Doctor</option>
                                                    <option value="">Dr. James Adult</option>
                                                    <option value="">Dr. James Alison</option>
                                                    <option value="">Dr. Peter Adlock</option>
                                                    <option value="">Dr. Jelin Alis</option>
                                                    <option value="">Dr. Josh Taylor</option>
                                                    <option value="">Dr. Steven Smith</option>
                                                </select>	
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option value="">Select Specialist</option>
                                                    <option value="">Cardiologists</option>
                                                    <option value="">Dermatologists</option>
                                                    <option value="">Endocrinologists</option>
                                                    <option value="">Gastroenterologists</option>
                                                    <option value="">Allergists</option>
                                                    <option value="">Immunologists</option>
                                                </select>	
                                            </div>
                                        </div>
    
                                        <div class="col-lg-6 col-sm-6">
                                            <div class="form-group">
                                                <input type="text" name="name" class="form-control" required data-error="Please enter your name" placeholder="Name">
                                            </div>
                                        </div>
        
                                        <div class="col-lg-6 col-sm-6">
                                            <div class="form-group">
                                                <input type="email" name="email" class="form-control" required data-error="Please enter your email" placeholder="Email">
                                            </div>
                                        </div>
        
                                        <div class="col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group date" id="datetimepicker">
                                                    <input type="text" class="form-control" placeholder="Select Time">
                                                    <span class="input-group-addon"></span>
                                                </div>	
                                            </div>
                                        </div>
        
                                        <div class="col-lg-12 col-md-12">
                                            <button type="submit" class="default-btn">
                                                Book An Appointment
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="appointment-img-2">
                <img src="frontend/img/appointment/appointment-img2.png" alt="Images">
            </div>
            <div class="appointment-shape">
                <img src="frontend/img/appointment/appointment-shape.png" alt="Images">
            </div>
        </div> -->
        <div class="subscribe-area ptb-100">
            <div class="subscribe-shape">
                <img src="frontend/img/subscribe-img/subscribe-shape.png" alt="Images">
            </div>
        </div>
@endsection