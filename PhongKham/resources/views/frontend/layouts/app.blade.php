<!doctype html>
<html lang="zxx">
    <head>
        <!-- Required Meta Tags -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Bootstrap CSS --> 
        <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}">
        <!-- Animate Min CSS -->
        <link rel="stylesheet" href="{{asset('frontend/css/animate.min.css')}}">
        <!-- Flaticon CSS --> 
        <link rel="stylesheet" href="{{asset('frontend/fonts/flaticon.css')}}">
        <!-- Boxicons CSS -->
        <link rel="stylesheet" href="{{asset('frontend/css/boxicons.min.css')}}">
        <!-- Magnific Popup CSS -->
        <link rel="stylesheet" href="{{asset('frontend/css/boxicons.min.css')}}">
        <!-- Owl Carousel Min CSS -->
        <link rel="stylesheet" href="{{asset('frontend/css/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/owl.theme.default.min.css')}}">
        <!-- Nice Select Min CSS --> 
        <!-- <link rel="stylesheet" href="{{asset('frontend/css/nice-select.min.css')}}"> -->
        <!-- Meanmenu CSS -->
        <link rel="stylesheet" href="{{asset('frontend/css/meanmenu.css')}}">
        <!-- Style CSS -->
        <link rel="stylesheet" href="{{asset('frontend/css/style.css')}}">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="{{asset('frontend/css/responsive.css')}}">
        <!-- Theme Dark CSS -->
        <link rel="stylesheet" href="{{asset('frontend/css/theme-dark.css')}}">

        <title>PHÒNG KHÁM 154</title>

        <!-- Favicon -->
        <link rel="icon" type="image/png" href="{{asset('frontend/img/logo154.png')}}">
        <style>
            img[src="https://cdn.000webhost.com/000webhost/logo/footer-powered-by-000webhost-white2.png"] {
                display: none;
            }
            img[alt="www.000webhost.com"]{display:none;}
        </style>
    </head>
    <body>
 

<!-- Your Plugin chat code -->

@include('frontend.layouts.header')
@yield('content')
@include('frontend.layouts.footer')

<div class="zalo-phone-ring-wrap">
	<div class="zalo-phone-ring">
		<div class="zalo-phone-ring-circle"></div>
		<div class="zalo-phone-ring-circle-fill"></div>
		<div class="zalo-phone-ring-img-circle">
		<a target="_blank" href="https://zalo.me/0934573996">				
                <img src="{{asset('frontend/img/zalo.png')}}">
            </a>
		</div>
	</div>
	
</div>

<div class="hotline-phone-ring-wrap">
	<div class="hotline-phone-ring">
		<div class="hotline-phone-ring-circle"></div>
		<div class="hotline-phone-ring-circle-fill"></div>
		<div class="hotline-phone-ring-img-circle">
		<a href="tel:0826154154" class="pps-btn-img">
			<img src="{{asset('frontend/img/icon-call-nh.png')}}" alt="Gọi điện thoại" width="50">
		</a>
		</div>
	</div>

</div>

       

       <!-- Jquery Min JS -->
        <script src="{{asset('frontend/js/jquery.min.js')}}"></script>
        <!-- Bootstrap Min JS -->
        <!-- <script src="{{asset('frontend/js/bootstrap.bundle.min.js')}}"></script> -->
        <!-- Magnific Popup Min JS -->
        <script src="{{asset('frontend/js/jquery.magnific-popup.min.js')}}"></script>
        <!-- Owl Carousel Min JS -->
        <script src="{{asset('frontend/js/owl.carousel.min.js')}}"></script>
        <!-- Nice Select Min JS -->
        <script src="{{asset('frontend/js/jquery.nice-select.min.js')}}"></script>
        <!-- Wow Min JS -->
        <script src="{{asset('frontend/js/wow.min.js')}}"></script>
        <!-- Meanmenu JS -->
        <script src="{{asset('frontend/js/meanmenu.js')}}"></script>
        <!-- Datepicker JS -->
        <script src="{{asset('frontend/js/datepicker.min.js')}}"></script>
        <!-- Ajaxchimp Min JS -->
        <script src="{{asset('frontend/js/jquery.ajaxchimp.min.js')}}"></script>
        <!-- Form Validator Min JS -->
        <script src="{{asset('frontend/js/form-validator.min.js')}}"></script>
        <!-- Contact Form JS -->
        <script src="{{asset('frontend/js/contact-form-script.js')}}"></script>
        <!-- Custom JS -->
        <script src="{{asset('frontend/js/custom.js')}}"></script>
        
        <!-- Messenger Plugin chat Code -->
                <div id="fb-root"></div>

            <!-- Your Plugin chat code -->
            <div id="fb-customer-chat" class="fb-customerchat">
            </div>

            <script>
            var chatbox = document.getElementById('fb-customer-chat');
            chatbox.setAttribute("page_id", "725026607607257");
            chatbox.setAttribute("attribution", "biz_inbox");
            </script>

            <!-- Your SDK code -->
            <script>
            window.fbAsyncInit = function() {
                FB.init({
                xfbml            : true,
                version          : 'v16.0'
                });
            };

            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
            </script>
            
    </body>
</html>