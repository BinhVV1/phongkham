<footer class="footer-area pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="footer-widget">
                    <h3>Thông Tin Liên Hệ</h3>
                    <p>Chúng tôi luôn ở bên cạnh bạn. Chúng tôi luôn sẵn sàng 24 giờ cho bạn trong trường hợp khẩn cấp.</p>
                    <ul class="footer-contact-list">
                        <li>
                            <i class='flaticon-pin'></i>
                            <div class="content">
                            144 Lê Lợi, Đông Hà, Quảng Trị, Việt Nam
                            </div>
                        </li>
                        <li>
                            <i class='flaticon-phone-call' style="margin-top:-5px"></i>
                            <div class="content">
                                <a href="tel:+84826154154">082 615 4154</a>
                            </div>
                        </li>
                        <li>
                            <i class='bx bxs-envelope'></i>
                            <div class="content">
                                <a href="mailto:Phongkham154@gmail.com">Phongkham154@gmail.com </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-2 col-md-6">
                <div class="footer-widget">
                    <h3>Danh Mục</h3>
                    <ul class="footer-list">
                        <li>
                            <a href="/" >
                                <i class='bx bxs-chevron-right'></i>
                                Trang Chủ
                            </a>
                        </li> 
                        <li>
                            <a href="{{route('about')}}">
                                <i class='bx bxs-chevron-right'></i>
                                Thông Tin
                            </a>
                        </li> 
                        <li>
                            <a href="{{route('services')}}" >
                                <i class='bx bxs-chevron-right'></i>
                                Dịch Vụ
                            </a>
                        </li> 
                        <li>
                            <a href="{{route('doctor')}}">
                                <i class='bx bxs-chevron-right'></i>
                                Bác Sĩ
                            </a>
                        </li> 
                        
                        <li>
                            <a href="{{route('contact')}}">
                                <i class='bx bxs-chevron-right'></i>
                               Liên Hệ
                            </a>
                        </li> 
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="footer-widget">
                    <h3>Thời Gian Làm Việc</h3>
                    <ul class="open-hours-list">
                        <li>
                            Thứ 2
                            <span>8:00 am - 9:30 pm</span>
                        </li> 
                        <li>
                        Thứ 3
                            <span>8:00 am - 9:30 pm</span>
                        </li> 
                        <li>
                        Thứ 4
                            <span>8:00 am - 9:30 pm</span>
                        </li> 
                        <li>
                        Thứ 5
                            <span>8:00 am - 9:30 pm</span>
                        </li> 
                        <li>
                        Thứ 6
                            <span>8:00 am - 9:30 pm</span>
                        </li> 
                        <li>
                        Thứ 7
                            <span>8:00 am - 9:30 pm</span>
                        </li> 
                        <li>
                            Chủ Nhật
                            <span>8:00 am - 9:30 pm</span>
                        </li> 
                    </ul>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="footer-widget ps-2">
                    <div class="footer-logo">
                        <a href="index.html">
                            <img src="{{asset('frontend/img/logo154.png')}}" class="footer-logo1" alt="Images">
                            <img src="{{asset('frontend/img/logo154.png')}}" class="footer-logo2" alt="Images">
                        </a>
                    </div>
                    <p><h3>Tận Tâm - Tận Lực - Trách Nhiệm</h3></p>
                    <ul class="social-link">
                        <li>
                            <a href="#" target="_blank"><i class='bx bxl-facebook'></i></a>
                        </li> 
                        <li>
                            <a href="#" target="_blank"><i class='bx bxl-twitter'></i></a>
                        </li> 
                        <li>
                            <a href="#" target="_blank"><i class='bx bxl-instagram'></i></a>
                        </li> 
                        <li>
                            <a href="#" target="_blank"><i class='bx bxl-pinterest-alt'></i></a>
                        </li> 
                        <li>
                            <a href="#" target="_blank"><i class='bx bxl-youtube'></i></a>
                        </li> 
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Area End -->

<!-- Copy-Right Area -->
<div class="copy-right-area">
    <div class="container">
        <div class="copy-right-text text-center">
            <p>
                Phòng Khám 154 
              
            </p>
        </div>
    </div>
</div>
<!-- Copy-Right Area End -->

<!-- Color Switch Button -->
<div class="switch-box">
    <label id="switch" class="switch">
        <input type="checkbox" onchange="toggleTheme()" id="slider">
        <span class="slider round"></span>
    </label>
</div>
<!-- Color Switch Button End -->
