     <!-- Pre Loader -->
     <div class="preloader">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="spinner"></div>
                </div>
            </div>
        </div>
        <!-- End Pre Loader -->

        <!-- Top Header Start -->
        <header class="top-header top-header-bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-8 col-md-8">
                        <div class="header-left">
                            <div class="header-left-card">
                                <ul>
                                    <li>
                                        <div class="head-icon">
                                            <i class='bx bx-mail-send'></i>
                                        </div>
                                        <a href="mailto:Phongkham154@gmail.com">Phongkham154@gmail.com</a>
                                    </li>
    
                                    <li>
                                        <div class="head-icon">
                                            <i class='bx bx-time-five'></i>
                                        </div>
                                        <a href="#">Thứ 2 - Chủ Nhật: 8:00 - 21:30</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <div class="header-right">
                            <div class="top-social-link">
                                <ul>
                                    <li>
                                        <a href="#" target="_blank">
                                            <i class='bx bxl-facebook'></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank">
                                            <i class='bx bxl-twitter'></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank">
                                            <i class='bx bxl-youtube'></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank">
                                            <i class='bx bxl-instagram'></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank">
                                            <i class='bx bxl-google-plus'></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Top Header End -->

        <!-- Start Navbar Area -->
        <div class="navbar-area">
            <!-- Menu For Mobile Device -->
            <div class="mobile-nav">
                <a href="/" class="logo">
                    <img src="{{asset('frontend/img/logo154.png')}}" class="logo-one" alt="Logo">
                    <img src="{{asset('frontend/img/logo154.png')}}" class="logo-two" alt="Logo">
                </a>
            </div>

            <!-- Menu For Desktop Device -->
            <div class="main-nav">
                <div class="container">
                    <nav class="navbar navbar-expand-md navbar-light ">
                        <a class="navbar-brand" href="/">
                            <img src="{{asset('frontend/img/logo154.png')}}" class="logo-one" alt="Logo">
                            <img src="{{asset('frontend/img/logo154.png')}}" class="logo-two" alt="Logo">
                        </a>

                        <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                            <ul class="navbar-nav m-auto">
                                <li class="nav-item">
                                    <a href="/" class="nav-link active">
                                        Trang Chủ
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('about')}}" class="nav-link">
                                        Thông Tin
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('doctor')}}" class="nav-link">
                                        Bác Sĩ 
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{route('services')}}" class="nav-link">
                                        Dịch Vụ    
                                    </a>
                                </li>

                                <!-- <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        Tin Tức
                                    </a>
                                </li> -->

                                <li class="nav-item">
                                    <a href="{{route('contact')}}" class="nav-link">
                                        Liên Hệ
                                    </a>
                                </li>
                            </ul>
                           
                            <div class="others-options d-flex align-items-center">
                                <div class="container mt-3">
                                    <div class="input-group mb-3">
                                      <input type="text" class="form-control" placeholder="Tìm Kiếm">
                                      <button class="btn btn-success" type="submit" style="padding-top: 10px;background-color: #1FA2FF;">
                                        <i class="bx bx-search"></i>
                                        </button> 
                                    </div> 
                                  </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>

            <div class="side-nav-responsive">
                <div class="container">
                    <div class="dot-menu">
                        <div class="circle-inner">
                            <div class="circle circle-one"></div>
                            <div class="circle circle-two"></div>
                            <div class="circle circle-three"></div>
                        </div>
                    </div>
                    
                    <div class="container" style="max-width:315px;">
                        <div class="side-nav-inner">
                            <div class="side-nav justify-content-center align-items-center">
                                <div class="side-item">
                                    <div class="option-item" style="margin-right: 5px;">
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Search">
                                            <button class="btn btn-success" type="submit" style="padding-top: 10px;background-color: #1FA2FF;">
                                              <i class="bx bx-search"></i>
                                            </button> 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Navbar Area -->