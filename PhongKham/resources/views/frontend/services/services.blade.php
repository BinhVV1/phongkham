@extends('frontend.layouts.app')
@section('content')
<div class="inner-banner inner-bg7">
            <div class="container">
                <div class="inner-title">
                    <h3> Dịch Vụ </h3>
                    <ul>
                        <li>
                            <a href="/">Trang Chủ</a>
                        </li>
                        <li> Dịch Vụ </li>
                    </ul>
                </div>
            </div>
            <div class="inner-banner-shape">
                <div class="shape1">
                    <img src="{{asset('frontend/img/inner-banner/inner-banner-shape1.png')}}" alt="Images">
                </div>
                <div class="shape2">
                    <img src="{{asset('frontend/img/inner-banner/inner-banner-shape2.png')}}" alt="Images">
                </div>
            </div>
        </div>
        <!-- Inner Banner End -->

        <!-- Service Area -->
        <section class="service-area pt-100 pb-70">
            <div class="container">
                <div class="section-title text-center">
                    <h2>Dịch vụ mà chúng tôi cung cấp
</h2>
                    <div class="section-icon">
                        <div class="icon">
                            <i class="flaticon-dna"></i>
                        </div>
                    </div>
                    <p>
                    Chúng tôi cung cấp các dịch vụ tuyệt vời cho sức khỏe tốt nhất của bạn. 
                    Dưới đây là một số dịch vụ được bao gồm để bạn hiểu rõ hơn rằng chúng tôi luôn ở bên bạn.
                    </p>
                </div>
                <div class="row pt-45">
                @if(!empty($data))
                    @foreach($data as $val)
                    <div class="col-lg-4 col-md-6">
                        <div class="service-card">
                            <a href="{{route('services-detail',['id'=>$val['id']])}}"><img src="frontend/img/services/{{$val['hinhanh']}}" alt="Images"></a>
                            <div class="service-content">
                                <div class="service-icon">
                                    <i class="flaticon-doctor"></i>
                                </div>
                                <h3><a href="{{route('services-detail',['id'=>$val['id']])}}">{{$val['tendichvu']}}</a></h3>
                                <div class="content">
                                    <p>{{$val['mota']}}</p>
                                </div>
                            </div>
                            <div class="service-shape-1">
                                <img src="frontend/img/services/service-shape1.png" alt="Images">
                            </div>
                            <div class="service-shape-2">
                                <img src="frontend/img/services/service-shape2.png" alt="Images">
                            </div>
                        </div>
                    </div>

                    @endforeach
                    @else
                    <p style='margin:auto;'> Hiện Chưa Có Danh Sách Dịch Vụ</p>
                    @endif
                        
                </div>
            </div>
            <div class="service-dots">
                <img src="{{asset('frontend/img/services/service-dots.png')}}" alt="">
            </div>
        </section>
        <!-- Service Area End -->

        <!-- Appointment Area -->
        
        <!-- Appointment Area End -->

        <!-- Banner Bottom Three -->
            <!-- <div class="banner-bottom-three pt-100 pb-70">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-sm-6">
                            <div class="banner-bottom-item">
                                <i class='flaticon-first-aid-kit'></i>
                                <h3>Tư Vấn</h3>
                                <p>Lorem ipsum dolor sit amet, con setetur sadipscing elitr, sed.</p>
                                <div class="circle"></div>
                                <div class="line"></div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6">
                            <div class="banner-bottom-item">
                                <i class='flaticon-fast-delivery'></i>
                                <h3>Fast Shipping</h3>
                                <p>Lorem ipsum dolor sit amet, con setetur sadipscing elitr, sed.</p>
                                <div class="circle"></div>
                                <div class="line"></div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 offset-lg-0 offset-sm-3">
                            <div class="banner-bottom-item">
                                <i class='flaticon-laptop'></i>
                                <h3>Online Shop</h3>
                                <p>Lorem ipsum dolor sit amet, con setetur sadipscing elitr, sed.</p>
                                <div class="circle"></div>
                                <div class="line"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        <!-- Banner Bottom Three End -->

        <!-- Case Study Area -->
        <div class="subscribe-area ptb-100">
            <div class="subscribe-shape">
                <!-- <img src="frontend/img/subscribe-img/subscribe-shape.png" alt="Images"> -->
            </div>
        </div>
@endsection