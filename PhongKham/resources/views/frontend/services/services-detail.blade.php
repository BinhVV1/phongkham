@extends('frontend.layouts.app')
@section('content')
<div class="inner-banner inner-bg6">
            <div class="container">
                <div class="inner-title">
                    <h3>{{$detail?$detail[0]['tendichvu']:''}}</h3>
                    <ul>
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li> <a href="{{route('services')}}"> Dịch Vụ</a> </li>
                        <li> {{$detail?$detail[0]['tendichvu']:''}}</li>
                    </ul>
                </div>
            </div>
            <div class="inner-banner-shape">
                <div class="shape1">
                    <img src="{{asset('frontend/img/inner-banner/inner-banner-shape1.png')}}" alt="Images">
                </div>
                <div class="shape2">
                    <img src="{{asset('frontend/img/inner-banner/inner-banner-shape2.png')}}" alt="Images">
                </div>
            </div>
        </div>
        <!-- Inner Banner End -->

        <!-- Service Details Area -->
        <div class="service-details-area pt-100 pb-70">
            <div class="container">
                <div class="row">
                    
                    <div class="col-lg-8">
                    @if(!empty($detail)||isset($detail))
                   
                        <div class="services-article">
                            <div class="services-article-img">
                                <img src="{{asset('frontend/img/services/'.$detail[0]['hinhanh'].'')}}" alt="Images">
                            </div>

                            <div class="services-content">
                                <!-- <h2>Medical Counselling</h2> -->
                                <p>
                                    {{$detail[0]['noidung']}}
                                </p>
                            </div>
                       
                       
                           

                            <!-- <div class="services-specialist">
                                <h2>Our Specialist For Medical Treatment</h2>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="doctors-item">
                                            <div class="doctors-img">
                                                <a href="doctors-details.html">
                                                    <img src="{{asset('frontend/img/doctors/doctors-img2.png')}}" alt="Images">
                                                </a>
                                            </div>
                                            <div class="content">
                                                <h3><a href="doctors-details.html">Dr. Peter Adlock</a></h3>
                                                <span>Orthopedics Surgeon </span> 
                                                <ul class="social-link">
                                                    <li>
                                                        <a href="#" target="_blank"><i class='bx bxl-facebook'></i></a>
                                                    </li> 
                                                    <li>
                                                        <a href="#" target="_blank"><i class='bx bxl-twitter'></i></a>
                                                    </li> 
                                                    <li>
                                                        <a href="#" target="_blank"><i class='bx bxl-instagram'></i></a>
                                                    </li> 
                                                    <li>
                                                        <a href="#" target="_blank"><i class='bx bxl-linkedin'></i></a>
                                                    </li> 
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-6 col-md-6">
                                        <div class="doctors-item">
                                            <div class="doctors-img">
                                                <a href="doctors-details.html">
                                                    <img src="{{asset('frontend/img/doctors/doctors-img3.png')}}" alt="Images">
                                                </a>
                                            </div>
                                            <div class="content">
                                                <h3><a href="doctors-details.html">Sinthy Alina</a></h3>
                                                <span>Gynecologist</span> 
                                                <ul class="social-link">
                                                    <li>
                                                        <a href="#" target="_blank"><i class='bx bxl-facebook'></i></a>
                                                    </li> 
                                                    <li>
                                                        <a href="#" target="_blank"><i class='bx bxl-twitter'></i></a>
                                                    </li> 
                                                    <li>
                                                        <a href="#" target="_blank"><i class='bx bxl-instagram'></i></a>
                                                    </li> 
                                                    <li>
                                                        <a href="#" target="_blank"><i class='bx bxl-linkedin'></i></a>
                                                    </li> 
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        @else
                        <p style='margin:auto;'> Hiện Chưa Có Danh Sách Dịch Vụ</p>
                        @endif
                    </div>
                    <div class="col-lg-4">
                        <div class="service-sidebar">
                            <!-- <div class="search-widget">
                                <form class="search-form">
                                    <input type="search" class="form-control" placeholder="Search...">
                                    <button type="submit">
                                        <i class="bx bx-search"></i>
                                    </button>
                                </form>
                            </div> -->

                            <div class="service-categories">
                                <h3>Những Dịch Vụ Khác</h3>
                                <ul>
                                @if(!empty($data2))
                                      @foreach($data2 as $val2)
                                    <li>
                                        <a href="{{route('services-detail',['id'=>$val2['id']])}}">
                                           {{$val2['tendichvu']}}
                                            <i class='bx bx-plus'></i>
                                        </a>
                                        
                                    </li>
                                    @endforeach
                                    @else
                                    <p style='margin:auto;'> Hiện Chưa Có Danh Sách Dịch Vụ</p>
                                    @endif
                                </ul>
                            </div>

                            <div class="service-open-hours">
                                <h3>Giờ Làm Việc</h3>
                                <ul>
                                    @for($i=2;$i<=7;$i++)
                                        <li>
                                            Thứ {{$i}}
                                            <span>8:00 am - 9:30 pm</span>
                                        </li> 
                                    @endfor
                                    <li>
                                        Chủ Nhật
                                        <span>8:00 am - 9:30 pm</span>
                                    </li> 
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="service-details-shape">
                    <img src="{{asset('frontend/img/services/service-dots.png')}}" alt="Images">
                </div>
            </div>
        </div>
        <!-- Service Details Area End -->

        <!-- Appointment Area -->
        <div class="subscribe-area ptb-100">
            <div class="subscribe-shape">
                <!-- <img src="frontend/img/subscribe-img/subscribe-shape.png')}}" alt="Images"> -->
            </div>
        </div>
        <!-- Appointment Area End -->

        <!-- Brand Area -->
        
@endsection