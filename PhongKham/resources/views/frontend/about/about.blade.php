@extends('frontend.layouts.app')
@section('content')
  <!-- Inner Banner -->
  <div class="inner-banner inner-bg1">
            <div class="container">
                <div class="inner-title">
                    <h3>Giới Thiệu</h3>
                    <ul>
                        <li>
                            <a href="/">Trang Chủ</a>
                        </li>
                        <li>Thông Tin</li>
                    </ul>
                </div>
            </div>
            <div class="inner-banner-shape">
                <div class="shape1">
                    <img src="frontend/img/inner-banner/inner-banner-shape1.png" alt="Images">
                </div>
                <div class="shape2">
                    <img src="frontend/img/inner-banner/inner-banner-shape2.png" alt="Images">
                </div>
            </div>
        </div>
        <!-- Inner Banner End -->

        <!-- About Area -->
        <div class="about-area pt-100 pb-70">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="about-right-img">
                            <img src="frontend/img/about-img/about-img2.jpg" alt="Images">
                            <div class="about-open-hours">
                                <h3>Thời Gian Làm Việc</h3>
                                <ul>
                                    @for($i=2;$i<=7;$i++)
                                        <li>
                                            Thứ {{$i}}
                                            <span>8:00 am - 9:30 pm</span>
                                        </li> 
                                   @endfor
                                    <li>
                                        Chủ Nhật
                                        <span>8:00 am - 9:30 pm</span>
                                    </li> 
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="about-content">
                            <div class="section-title">
                                <span>Giới Thiệu</span>
                                <h2>Chúng tôi là người bạn đáng tin cậy của bạn</h2>
                                <p>Phòng khám 154 là một nơi đáng tin cậy của Dịch vụ y tế luôn ở bên cạnh bạn và sức khỏe của bạn là ưu tiên hàng đầu của chúng tôi.</p>
                        <p>
                        Phòng khám 154 sẽ được quản lý thông qua các chương trình có thể tùy chỉnh dựa trên kế hoạch kết hợp sự hợp tác giữa các thành viên gia đình và người chăm sóc đối với bệnh dài hạn hoặc quản lý bệnh.
                        </p>
                            </div>
                            <div class="about-card">
                                <i class='flaticon-24-hours bg-three'></i>
                                <div class="content">
                                    <span>Hỗ trợ 24/7</span>
                                    <p>Our medical team of  different department for long term illness writers and editors makes all the </p>
                                </div>
                            </div>

                            <div class="about-card">
                                <i class='flaticon-ambulance-2 bg-three'></i>
                                <div class="content">
                                    <span>Hỗ trợ khẩn cấp</span>
                                    <p>Our medical team of  different department for long term illness writers and editors makes all the</p>
                                </div>
                            </div>

                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About Area End -->

        <!-- Brand Area -->
      
        <!-- Brand Area End -->

        <!-- Doctors Area -->
       
        <!-- Doctors Area End -->
        
        <!-- Serve Area End -->
        <div class="serve-area ptb-100">
            <div class="container">
                <div class="section-title text-center">
                    <h2>Xem cách chúng tôi phục vụ bệnh nhân</h2>
                    <div class="section-icon">
                        <div class="icon">
                            <i class="flaticon-dna"></i>
                        </div>
                    </div>
                </div>

                <div class="serve-btn-area">
                        <div class='col-sm-12'>
                            <video controls style='width:90%;'>
                            <source src="frontend/img/serve/serve-img.mp4">
                            </video>
                        </div>  
                    <!-- <a href="https://www.youtube.com/watch?v=K4DyBUG242c" class="play-btn"><i class='bx bx-play'></i></a> -->
                </div>
            </div>
        </div>
        <!-- Serve Area End -->
        <div class="doctors-area">
            <div class="container">
                <div class="section-title text-center">
                    <h2>Gặp gỡ chuyên gia của chúng tôi</h2>
                    <div class="section-icon">
                        <div class="icon">
                            <i class="flaticon-dna"></i>
                        </div>
                    </div>
                    <p>
                    Chúng tôi cung cấp các dịch vụ tuyệt vời cho sức khỏe tốt nhất của bạn. <br>
                    Dưới đây là một số chuyên gia của chúng tôi.


                    </p>
                </div>
                <div class="doctors-slider owl-carousel owl-theme pt-45">
                    @if(!empty($data)||isset($data))
                        @foreach($data as $val)
                        <div class="doctors-item">
                        <div class="doctors-img">
                            <a href="{{route('doctor-detail',['id'=>$val['id']])}}">
                                <img src="frontend/img/doctors/{{$val['hinhanh']}}" alt="Images">
                            </a>
                        </div>
                        <div class="content">
                            <h3><a href="{{route('doctor-detail',['id'=>$val['id']])}}">{{$val['tenbacsi']}}</a></h3>
                            <span>{{$val['bangcap']}} </span> 
                            <ul class="social-link">
                                <li>
                                    <a href="#" target="_blank"><i class='bx bxl-facebook'></i></a>
                                </li> 
                                <li>
                                    <a href="#" target="_blank"><i class='bx bxl-twitter'></i></a>
                                </li> 
                                <li>
                                    <a href="#" target="_blank"><i class='bx bxl-instagram'></i></a>
                                </li> 
                                <li>
                                    <a href="#" target="_blank"><i class='bx bxl-linkedin'></i></a>
                                </li> 
                            </ul>
                        </div>
                    </div>
                        @endforeach
                    @else
                    <span>Hiện chưa có danh sách bác sĩ</span>
                    @endif
                   
                </div>
            </div>

            <div class="doctors-shape">
                <div class="doctors-shape-1">
                    <img src="frontend/img/doctors/doctors-shape1.png" alt="Images">
                </div>
                <div class="doctors-shape-2">
                    <img src="frontend/img/doctors/doctors-shape2.png" alt="Images">
                </div>
            </div>
        </div>
        <!-- Testimonials Area -->
        <div class="testimonials-area pt-100 pb-70">
            <div class="container">
                <div class="section-title text-center">
                    <span>Đánh Giá</span>
                    <h2>Suy nghĩ của bệnh nhân của chúng tôi</h2>
                </div>
                <div class="row pt-45">
                    <div class="col-lg-6">
                        <div class="testimonials-img">
                            <img src="frontend/img/testimonials/testimonials-img.jpg" alt="Images">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="testimonials-slider-area">
                            <div class="testimonials-slider owl-carousel owl-theme">
                                <div class="testimonials-item">
                                    <i class="flaticon-left-quote"></i>
                                    <p>Medizo hospital for their professional competent and committed approach they deliver at all times, making me and other patients feel at ease during these anxious times. </p>
                                    <div class="content">
                                        <img src="frontend/img/testimonials/testimonials-client1.jpg" alt="Images">
                                        <h3>Sinthy Alina</h3>
                                        <span>Heart Patient</span>
                                    </div>
                                </div>

                                <div class="testimonials-item">
                                    <i class="flaticon-left-quote"></i>
                                    <p>Medizo hospital for their professional competent and committed approach they deliver at all times, making me and other patients feel at ease during these anxious times. </p>
                                    <div class="content">
                                        <img src="frontend/img/testimonials/testimonials-client2.jpg" alt="Images">
                                        <h3>Evanaa</h3>
                                        <span>Diabetic Patient</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Testimonials Area End -->

        <!-- Emergency Area Two -->
        <div class="emergency-area-two ptb-100">
            <div class="container">
                <div class="emergency-area-bg">
                    <div class="row align-items-center">
                        <div class="col-lg-7 col-md-6">
                            <div class="emergency-content emergency-content-ptb">
                                <h2>Nhận dịch vụ chăm sóc <b> khẩn cấp 24/7 </b> </h2>
                                <p>Chúng tôi luôn ở bên cạnh bạn. Chúng tôi luôn sẵn sàng 24 giờ cho bạn trong trường hợp khẩn cấp.</p>
                                <div class="emergency-icon-content">
                                    <i class="flaticon-24-hours-1"></i>
                                    <h3><a href="tel:+84826154154">082 615 4154</a></h3>
                                </div>
                            </div>
                        </div>
    
                        <div class="col-lg-5 col-md-6">
                            <div class="emergency-img">
                               <img src="frontend/img/emergency/emergency-img.png" alt="Images">
                            </div>
                        </div>
                    </div>
                    <div class="emergency-shape-2">
                        <img src="frontend/img/emergency/emergency-shape.png" alt="Images">
                    </div>
                </div>
            </div>
        </div>
        <!-- Emergency Area Two End -->

        <!-- Footer Area -->
@endsection