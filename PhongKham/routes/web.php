<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\ServicesController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class,'index']);
Route::get('/about',[AboutController::class,'index'])->name('about');
Route::get('/doctor',[DoctorController::class,'index'])->name('doctor');
Route::get('/doctor-detail/{id}',[DoctorController::class,'detail'])->name('doctor-detail');

Route::get('/services',[ServicesController::class,'index'])->name('services');
Route::get('/services-detail/{id}',[ServicesController::class,'detail'])->name('services-detail');

Route::get('/contact',[HomeController::class,'contact'])->name('contact');
